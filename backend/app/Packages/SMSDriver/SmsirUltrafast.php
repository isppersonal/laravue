<?php

namespace App\Packages\SMSDriver;

use Illuminate\Support\Facades\Log;
use Tzsk\Sms\Drivers\Smsir;

class SmsirUltrafast extends Smsir
{

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function send()
    {
        $token = $this->getToken();
        $response = collect();

        foreach ($this->recipients as $recipient) {
            $result = $this->client->request(
                'POST',
                data_get($this->settings, 'url') . '/api/UltraFastSend',
                $this->fastPayload($recipient, $token)
            );
            $response->put($recipient, $result);
        }

        return (count($this->recipients) == 1) ? $response->first() : $response;
    }
    /**
     * @param string $recipient
     * @param string $token
     * @return array
     */
    protected function fastPayload($recipient, $token)
    {
        $body = json_decode($this->body, true);
        $payload = [
            'json' => [
                'ParameterArray' => $body['params'],
                'TemplateId' => $body['template_id'],
                'Mobile' => $recipient,
            ],
            'headers' => [
                'x-sms-ir-secure-token' => $token,
            ],
            'connect_timeout' => 30,
        ];

        Log::debug(json_encode($payload, true));

        return $payload;
    }
}
