<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use Kavenegar\Laravel\Message\KavenegarMessage;
use Kavenegar\Laravel\Notification\KavenegarBaseNotification;
use Rezahmady\Smsir\SmsirChannel;
use Rezahmady\Smsir\SmsirMessage;
use Tzsk\Sms\Builder;
use Tzsk\Sms\Channels\SmsChannel;

class UserCreated extends Notification
{
    use Queueable;

    public function via($notifiable)
    {
        return [SmsirChannel::class];
    }

    public function toSmsir($notifable)
    {
        Log::debug($notifable->verify_code);
        return (new SmsirMessage())
            ->setMethod('ultraFastSend')
            ->setCode($notifable->verify_code);
//            ->setMethod('ultraFastSend')
//            ->setTemplateId('36180')
//            ->setParameters(['VerificationCode' => $notifable->verify_code]);
    }

//    public function toSms($notifiable)
//    {
//        $params = [
//            ['Parameter' => 'VerificationCode', 'ParameterValue' => $this->user->verify_code]
//        ];
//
//        $payload = [
//            'params' => $params,
//            'template_id' => 36180
//        ];
//        return (new Builder)
//            ->via('smsirultrafast')
//            ->to($this->user->mobile)
//            ->send(json_encode($payload, true));
//    }
}
