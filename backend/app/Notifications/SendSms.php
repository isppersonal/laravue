<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Tzsk\Sms\Builder;
use Tzsk\Sms\Channels\SmsChannel;

class SendSms extends Notification
{
    use Queueable;

    public $to;

    public $template;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($template, $to)
    {
        $this->to = $to;
        $this->template = $template;
    }

    public function via($notifable)
    {
        return [SmsChannel::class];
    }

    public function toSms()
    {
        return (new Builder)->send();
    }
}
