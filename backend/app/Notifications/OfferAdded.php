<?php

namespace App\Notifications;

use App\Models\Opportunity;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Kavenegar\Laravel\Message\KavenegarMessage;
use Kavenegar\Laravel\Notification\KavenegarBaseNotification;

class OfferAdded extends KavenegarBaseNotification
{
    use Queueable;

    public Opportunity $offer;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Opportunity $offer)
    {
        $this->offer = $offer;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     */
    public function toKavenegar($notifiable)
    {
        Log::debug($this->offer->title);
        return (new KavenegarMessage())->verifyLookup('Verify', [
            $this->offer->id
        ]);
    }
}
