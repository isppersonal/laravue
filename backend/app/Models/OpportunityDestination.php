<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OpportunityDestination extends Model
{
    use HasFactory;

    protected $fillable = [
        'opportunity_id',
        'city_id'
    ];

    protected $with = ['city'];

    public function opportunity()
    {
        return $this->belongsTo(Opportunity::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
