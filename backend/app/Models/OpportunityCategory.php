<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OpportunityCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'opportunity_id',
        'category_id'
    ];

    public function opportunity()
    {
        return $this->belongsTo(Opportunity::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
