<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Article extends Model
{
    use HasFactory, HasSlug;

    public const STATUS_PUBLISHED = 1;
    public const STATUS_DRAFT = 0;

    protected $with = ['tags', 'categories', 'author'];

    protected $fillable = [
        'title',
        'user_id',
        'content',
        'status',
        'published_at',
        'like_count',
        'view_count'
    ];

    protected $dates = ['published_at'];

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->usingLanguage('');
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function categories()
    {
        return $this->belongsToMany(ArticleCategory::class, 'article_category', 'article_id', 'category_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'article_tag');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class)->orderBy('id', 'desc');
    }

    public function attachments()
    {
        return $this->morphMany(Asset::class, 'attachable');
    }

    public static function scopePublished($query)
    {
        return $query->where('status', self::STATUS_PUBLISHED);
    }
    public static function scopeDraft($query)
    {
        return $query->where('status', self::STATUS_DRAFT);
    }

//    public static function create(array $attributes = [])
//    {
//        $attributes['user_id'] = Auth::user()->id ?? 1;
//
//        return static::query()->create($attributes);
//    }
}
