<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Opportunity extends Model
{
    use HasFactory;

    protected $with = ['destinations', 'origin', 'user'];

    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 1;

    protected $fillable = [
        'title',
        'user_id',
        'category_id',
        'origin_id',
        'description',
        'status'
    ];

//    protected $appends = ['destination'];

    public function getDestinationAttribute()
    {
        return $this->destinations()->orderBy('id', 'desc')->get()->toArray();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function origin()
    {
        return $this->hasOne(City::class, 'id', 'origin_id');
    }

    public function destinations()
    {
        return $this->hasMany(OpportunityDestination::class);
    }

    public function categories()
    {
        return $this->hasMany(OpportunityCategory::class);
    }

    public function offers()
    {
        return $this->hasMany(OpportunityOffer::class);
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }
}
