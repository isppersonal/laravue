<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OpportunityOffer extends Model
{
    use HasFactory;

    // waiting - viewed - accepted - rejected
    public const STATUS_WAITING = 0;
    public const STATUS_VIEWED = 1;
    public const STATUS_ACCEPTED = 2;
    public const STATUS_REJECTED = 3;

    protected $fillable = [
        'opportunity_id',
        'offer_id',
        'status',
        'description'
    ];

    protected $with = ['opportunity', 'offer'];

    public function opportunity()
    {
        return $this->belongsTo(Opportunity::class);
    }

    public function offer()
    {
        return $this->belongsTo(Opportunity::class, 'offer_id', 'id');
    }

    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }
}
