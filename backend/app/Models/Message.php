<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $guarded = [];

    public const STATUS_PENDING = 0;
    public const STATUS_REPLIED = 1;
    public const STATUS_REJECTED = 2;

    public static function create(array $attributes)
    {
        if (!isset($attributes['status'])) {
            $attributes['status'] = self::STATUS_PENDING;
        }

        return static::query()->create($attributes);
    }
}
