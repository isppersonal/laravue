<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PaginateApi
{
    /**
     * Handle an incoming request.
     * remove extra paginate stuff from response (useful for api)
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        if ($response instanceof JsonResponse) {
            $data = $response->getData(true);

            if (isset($data['links'])) {
                unset($data['links']);
            }
            if (isset($data['meta'], $data['meta']['links'])) {
                unset($data['meta']['links']);
            }

            $response->setData($data);
        }

        return $response;
    }
}
