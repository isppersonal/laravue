<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile' => 'required|regex:/(09)[0-9]{9}/|unique:users,mobile',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
//            'national_code' => 'nullable|unique:users,national_code'
        ];
    }
}
