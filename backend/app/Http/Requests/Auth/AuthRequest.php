<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Validator;

class   AuthRequest extends FormRequest
{
    protected $model;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => 'required|exists:users,mobile|regex:/(09)[0-9]{9}/',
            'password' => 'required'
        ];
    }

    public function withValidator(Validator $validator)
    {
        $this->model = new User();

        $validator->after(function(Validator $validator) {
            $data = $validator->getData();

            $user = $this->model->where('mobile', $data['login'])->first();

            if ($user && $user->mobile_verified_at === null) {
                $validator->errors()->add('login', __('auth.unverified'));
            }
            if ($user && $user->status === $this->model::STATUS_INACTIVE) {
                $validator->errors()->add('login', __('auth.inactive'));
            }
        });
    }
}
