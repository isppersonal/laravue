<?php

namespace App\Http\Requests\Blog;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'article_id' => 'required|exists:articles,id',
            'subject' => 'string',
            'content' => 'required|min:10',
            'author_name' => 'required',
            'author_email' => 'required',
            'parent_id' => 'sometimes|exists:comments,id',
            'user_id' => 'sometimes|exists:users,id'
        ];
    }
}
