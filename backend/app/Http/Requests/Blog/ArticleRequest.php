<?php

namespace App\Http\Requests\Blog;

use App\Models\Article;
use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'content' => 'required|string',
            'tags.*' => 'sometimes|string',
            'categories.*' => 'sometimes|exists:article_categories,id',
            'status' => 'sometimes|in:' . implode(',', [Article::STATUS_DRAFT, Article::STATUS_PUBLISHED])
        ];
    }
}
