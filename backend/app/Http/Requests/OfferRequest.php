<?php

namespace App\Http\Requests;

use App\Models\Opportunity;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Validator;

class OfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'offer_id' => 'required|integer|exists:opportunities,id',
            'description' => 'sometimes|string'
        ];
    }
    /**
     * Configure the validator instance.
     *
     * @param  Validator  $validator
     * @return void
     */
    public function withValidator(Validator $validator)
    {
        $validator->after(function (Validator $validator) {
            $data = $validator->getData();
            if (!isset($data['offer_id'])) {
                return ;
            }

            $offer = Opportunity::find($data['offer_id']);
            $opportunity = $this->route('opportunity');
            $possible = $opportunity
                ->destinations()
                ->where('city_id', $offer->origin->id)
                ->first();
            if (!$possible) {
                $validator->errors()->add('offer.origin_id', __('origin not in destinations'));
            }
            $exists = $opportunity
                ->offers()
                ->where('offer_id', $offer->id)
                ->first();
            if ($exists) {
                $validator->errors()->add('offer_id', __('this offer already exists'));
            }
        });
    }
}
