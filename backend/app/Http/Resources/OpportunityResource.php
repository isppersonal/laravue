<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OpportunityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'position' => $this->position,
            'description' => $this->description,
            'owner' => $this->whenLoaded('user', function() {
                return [
                    'id' => $this->user->id,
                    'name' => $this->user->name
                ];
            }),
            'origin' => $this->whenLoaded('origin', function () {
                return [
                    'id' => $this->origin->id,
                    'name' => $this->origin->name
                ];
            }),
            'destinations' => DestinationResource::collection($this->whenLoaded('destinations')),
            'categories' => OpportunityCategoryResource::collection($this->whenLoaded('categories')),
            'offers' => OfferResource::collection($this->whenLoaded('offers')),
            'created_at' => $this->created_at->toIso8601String()
        ];
    }
}
