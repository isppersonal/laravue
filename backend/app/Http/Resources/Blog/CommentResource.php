<?php

namespace App\Http\Resources\Blog;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'subject' => $this->subject,
            'content' => $this->content,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'answers' => CommentResource::collection($this->whenLoaded('answers')),
            'parent' => new CommentResource($this->whenLoaded('parent')),
            'user' => new UserResource($this->whenLoaded('user')),
            'article' => new ArticleResource($this->whenLoaded('article')),
            'author' => [
                'email' => $this->author_email,
                'name' => $this->author_name
            ]
        ];
    }
}
