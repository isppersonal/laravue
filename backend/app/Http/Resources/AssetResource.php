<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AssetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'path' => $this->path,
            'mime' => $this->mime,
            'links' => [
                'full' => url('api/assets/'.$this->id.'/render'),
                'thumb' => url('api/assets/'.$this->id.'/render?width=200&height=200'),
            ],
            'created_at' => $this->created_at->toIso8601String(),
        ];
    }
}
