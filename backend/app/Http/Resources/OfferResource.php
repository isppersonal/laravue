<?php

namespace App\Http\Resources;

use App\Http\Resources\Tiny\OpportunityResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OfferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            'id' => $this->id,
            'description' => $this->description,
            'opportunity' => new OpportunityResource($this->whenLoaded('opportunity')),
            'offer' => new OpportunityResource($this->whenLoaded('offer')),
            'status' => $this->status,
            'created_at' => $this->created_at->toIso8601String(),
            'updated_at' => $this->updated_at->toIso8601String()
        ];
    }
}
