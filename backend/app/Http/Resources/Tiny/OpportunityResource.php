<?php

namespace App\Http\Resources\Tiny;

use App\Http\Resources\CityResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OpportunityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'owner' => $this->whenLoaded('user', function() {
                return $this->user->pluck('id', 'name');
            }),
            'origin' => new CityResource($this->whenLoaded('origin', function () {
                return $this->origin->pluck('id', 'name');
            }))
        ];
    }
}
