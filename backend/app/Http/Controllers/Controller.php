<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function isAdmin()
    {
        $user = Auth::user();
        if ($user) {
            return (bool) $user->is_admin;
        }
        return response()->json(['message' => __('auth.failed')], Response::HTTP_UNAUTHORIZED);
    }

    public function response_ok($data, $status = Response::HTTP_OK, $extraHeaders = [])
    {
        return response()->json([
            'data' => $data
        ], $status);
    }

    public function response_error($message, $errors = [], $status = Response::HTTP_INTERNAL_SERVER_ERROR, $extraHeaders = [])
    {
        return response()->json([
            'message' => $message,
            'errors' => $errors
        ], $status, $extraHeaders);
    }

    public function no_content()
    {
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }

}
