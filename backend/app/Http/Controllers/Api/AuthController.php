<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\AuthRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\VerifyRequest;
use App\Http\Requests\Auth\RecoverRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Notifications\UserCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

/**
 * @group Authentication
 *
 * API endpoints for managing authentication
 */
class AuthController extends Controller
{
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @unauthenticated
     *
     * @param AuthRequest $request
     * @return \Illuminate\Http\JsonResponse|void
     * @throws ValidationException
     */
    public function login(AuthRequest $request)
    {
        $request->validated();

        $user = $this->model->where('mobile', $request->login)->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'login' => __('auth.failed'),
            ]);
        }

        Auth::login($user);

        if(Auth::user()) {
            session()->regenerate();

            return $this->response_ok([
                'token' => $user->createToken('api-token', ['role:customer'])->plainTextToken,
                'ttl' => 1440,
                'info' => new UserResource($user)
            ]);
        }
    }

    /**
     * @unauthenticated
     *
     * @param RegisterRequest $request
     * @return UserResource
     * @throws \Exception
     */
    public function register(RegisterRequest $request)
    {
        $data = $request->validated();

        $user = $this->model->create($data);
        $user->verify_code = $this->code();

        $result = $user->notify(new UserCreated());
        Log::debug(json_encode($request));
        $user->save();

        return new UserResource($user->fresh());
    }

    /**
     * @unauthenticated
     *
     * @param RecoverRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function recover(RecoverRequest $request)
    {
        $request->validated();

        $this->model->where('mobile', $request->mobile)
            ->update([
                'mobile_verified_at' => null,
                'verify_code' => $this->code()
            ]);

        return $this->response_ok(['mobile' => $request->mobile]);
    }

    /**
     * @unauthenticated
     *
     * @param VerifyRequest $request
     * @return UserResource
     * @throws ValidationException
     */
    public function verify(VerifyRequest $request)
    {
        $request->validated();
        $user = $this->model->where('mobile' ,$request->mobile)->first();

        if (!$user || $user->verify_code !== (int) $request->verify_code) {
            throw ValidationException::withMessages([
                'verify_code' => __('auth.invalid_verify'),
            ]);
        }

        $user->update([
            'mobile_verified_at' => now(),
            'verify_code' => null,
            'password' => Hash::make($request->plain_password)
        ]);

        return new UserResource($user->fresh());
    }

    /**
     * @param Request $request
     * @return UserResource
     */
    public function profile(Request $request)
    {
        return new UserResource($request->user());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        auth()->user()->tokens()->delete();
        Auth::guard('api')->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return $this->no_content();
    }

    /**
     * @throws \Exception
     */
    private function code() {
        do {
            $verify = random_int(10000, 99999);
        } while ($this->model->where('verify_code', $verify)->count());
        return $verify;
    }
}
