<?php

namespace App\Http\Controllers\Api\Blog;

use App\Http\Controllers\Controller;
use App\Http\Requests\Blog\CommentRequest;
use App\Http\Requests\QueryRequest;
use App\Http\Resources\Blog\CommentResource;
use App\Models\Comment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CommentController extends Controller
{
    protected $model;

    public function __construct(Comment $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index(QueryRequest $request)
    {
        $comments = $this->model->paginate($request->limit);

        return CommentResource::collection($comments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CommentRequest $request
     * @return CommentResource
     */
    public function store(CommentRequest $request)
    {
        $data = $request->validated();

        $comment = $this->model::create($data);

        return new CommentResource($comment->fresh()->loadMissing([
            'user',
            'article'
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param Comment $comment
     * @return CommentResource
     */
    public function show(Comment $comment)
    {
        return new CommentResource($comment->loadMissing('answers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CommentRequest $request
     * @param Comment $comment
     * @return CommentResource
     */
    public function update(CommentRequest $request, Comment $comment)
    {
        $data = $request->validated();

        $comment->update($data);

        return new CommentResource($comment->fresh());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Comment $comment
     * @return JsonResponse
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();

        return $this->no_content();
    }
}
