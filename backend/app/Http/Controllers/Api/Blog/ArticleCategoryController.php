<?php

namespace App\Http\Controllers\Api\Blog;

use App\Http\Controllers\Controller;
use App\Http\Requests\Blog\CategoryRequest;
use App\Http\Requests\QueryRequest;
use App\Models\ArticleCategory;
use Illuminate\Http\Request;
use App\Http\Resources\Blog\ArticleCategoryResource as Resource;
/**
 * @group BlogArticleCategory
 *
 * API endpoints for managing Blog categories
 */
class ArticleCategoryController extends Controller
{
    protected $model;

    public function __construct(ArticleCategory $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(QueryRequest $request)
    {
        $categories = $this->model->paginate($request->limit);

        return Resource::collection($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryRequest $request
     * @return Resource
     */
    public function store(CategoryRequest $request)
    {
        $category = $this->model->create($request->validated());

        return new Resource($category);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArticleCategory  $articleCategory
     * @return Resource
     */
    public function show(ArticleCategory $articleCategory)
    {
        return new Resource($articleCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoryRequest $request
     * @param \App\Models\ArticleCategory $articleCategory
     * @return Resource
     */
    public function update(CategoryRequest $request, ArticleCategory $articleCategory)
    {
        $articleCategory->update($request->validated());

        return new Resource($articleCategory->fresh());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArticleCategory  $articleCategory
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(ArticleCategory $articleCategory)
    {
        foreach ($articleCategory->articles() as $article) {
            $article->categories()->where('category_id', $articleCategory->id)->delete();
        }

        $articleCategory->delete();

        return $this->no_content();
    }
}
