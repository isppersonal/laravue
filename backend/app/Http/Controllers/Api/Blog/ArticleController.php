<?php

namespace App\Http\Controllers\Api\Blog;

use App\Http\Controllers\Controller;
use App\Http\Requests\Blog\ArticleRequest;
use App\Http\Requests\QueryRequest;
use App\Models\Article;
use App\Models\Tag;
use App\Http\Resources\Blog\ArticleResource as Resource;
use Illuminate\Support\Facades\Log;

class ArticleController extends Controller
{
    protected $model;


    public function __construct(Article $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(QueryRequest $request)
    {
        $articles = $this->model->paginate($request->limit);

        return Resource::collection($articles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ArticleRequest $request
     * @return Resource
     */
    public function store(ArticleRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = 1;

        $article = $this->model->create($data);

        $article->tags()->sync($this->tagIdsForSync($request));
        $article->categories()->sync((array) $request->categories);

        return new Resource($article->fresh()->loadMissing('tags', 'categories', 'author'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return Resource
     */
    public function show(Article $article)
    {
        return new Resource($article->loadMissing([
            'tags',
            'comments',
            'comments.user',
            'categories',
            'author'
        ])
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ArticleRequest $request
     * @param \App\Models\Article $article
     * @return Resource
     */
    public function update(ArticleRequest $request, Article $article)
    {
        $article->update($request->validated());

        $article->tags()->sync($this->tagIdsForSync($request));
        $article->categories()->sync((array) $request->categories);

        return new Resource($article->fresh()->loadMissing([
            'tags',
            'categories',
            'comments',
            'comments.user',
            'author'
        ]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Article $article)
    {
        $article->delete();

        return $this->no_content();
    }

    protected function tagIdsForSync(ArticleRequest $request) {
        $tag_ids = [];

        foreach ($request->tags as $tag) {
            $tag = Tag::firstOrCreate(['title' => $tag]);
            if ($tag) {
                $tag_ids[] = $tag->id;
            }
        }

        return $tag_ids;
    }
}
