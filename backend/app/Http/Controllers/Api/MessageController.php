<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\MessageRequest;
use App\Http\Requests\QueryRequest;
use App\Http\Resources\MessageResource;
use App\Models\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    protected $model;

    public function __construct(Message $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(QueryRequest $request)
    {
        $messages = $this->model->paginate($request->limit);
        return MessageResource::collection($messages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return MessageResource
     */
    public function store(MessageRequest $request)
    {
        $data = $request->validated();

        $message = $this->model::create($data);

        return new MessageResource($message->fresh());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Message  $message
     * @return MessageResource
     */
    public function show(Message $message)
    {
        return new MessageResource($message);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MessageRequest $request
     * @param \App\Models\Message $message
     * @return MessageResource
     */
    public function update(MessageRequest $request, Message $message)
    {
        $message->update($request->validated());

        return new MessageResource($message->fresh());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Message $message)
    {
        $message->delete();

        return $this->no_content();
    }
}
