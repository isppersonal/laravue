<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProvinceRequest;
use App\Http\Resources\ProvinceResource;
use App\Models\Province;
use Illuminate\Http\Request;

class ProvinceController extends Controller
{    protected $model;

    public function __construct(Province $model)
    {
        $this->model = $model;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $provinces = $this->model->all();

        return ProvinceResource::collection($provinces);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProvinceRequest $request
     * @return ProvinceResource
     */
    public function store(ProvinceRequest $request)
    {
        $province = $this->model->create($request->validated());

        return new ProvinceResource($province);
    }

    /**
     * Display the specified resource.
     *
     * @param Province $province
     * @return ProvinceResource
     */
    public function show(Province $province)
    {
        return new ProvinceResource($province);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProvinceRequest $request
     * @param Province $province
     * @return ProvinceResource
     */
    public function update(ProvinceRequest $request, Province $province)
    {
        $province->update($request->validated());

        return new ProvinceResource($province->fresh());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Province $province
     * @return \Illuminate\Http\Response
     */
    public function destroy(Province $province)
    {
        // TODO : what happen to city that in rel with this
    }
}
