<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\OfferRequest;
use App\Http\Resources\OfferResource;
use App\Http\Resources\UserResource;
use App\Models\Opportunity;
use App\Models\OpportunityOffer;
use App\Notifications\OfferAdded;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * @group Offers
 *
 * API endpoints for managing opportunity offers
 */
class OfferController extends Controller
{
    protected $model;

    public function __construct(OpportunityOffer $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Log::debug(json_encode($this->user));

//        return new UserResource($this->user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OfferRequest $request
     * @return OfferResource
     */
    public function store(OfferRequest $request, Opportunity $opportunity)
    {
        $data = $request->validated();
        $offer = $opportunity->offers()->create($data);
//        $opportunity->user->notify(new OfferAdded($opportunity));

        return new OfferResource($offer);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OpportunityOffer  $opportunityOffer
     * @return OfferResource
     */
    public function show(OpportunityOffer $opportunityOffer)
    {
        return new OfferResource($opportunityOffer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OpportunityOffer  $opportunityOffer
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(OpportunityOffer $opportunityOffer)
    {
        $opportunityOffer->delete();
        return $this->no_content();
    }
}
