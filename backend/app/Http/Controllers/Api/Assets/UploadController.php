<?php

namespace App\Http\Controllers\Api\Assets;

use App\Http\Controllers\Controller;
use App\Http\Resources\AssetResource;
use App\Models\Asset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;

class UploadController extends Controller
{
    protected $model;

    public function __construct(Asset $model)
    {
        $this->model = $model;
    }

    public function store(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $path = $this->storeInFileSystem($file);

            return $this->storeInDB($file, $path, $request->user());
        }
    }

    protected function storeInDB(UploadedFile $file, $path, $user)
    {
        $file = $user->avatar()->create([
            'path' => $path,
            'type' => $file->getClientMimeType(),
            'mime' => $file->getMimeType(),
            'user_id' => $user->id
        ]);
        return new AssetResource($file);
    }

    protected function storeInFileSystem(UploadedFile $file) {
        $name = md5(Str::random(16).date('U'))
            . '.' . $file->getClientOriginalExtension();
        return $file->storeAs(
            'assets' . DIRECTORY_SEPARATOR . 'users',
            $name
        );
    }

    public function destroy(Asset $asset)
    {
        if (Storage::delete($asset->path)) {
            $asset->delete();
        }
        return $this->no_content();
    }
}
