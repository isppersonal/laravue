<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;


/**
 * @group Users
 *
 * API endpoints for managing Users
 */
class UserController extends Controller
{
    protected $model;

    public function __constructor(User $model)
    {
        $this->model = $model;
    }
    /**
     * Display a listing of the users.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     *
     * @apiResourceCollection App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User
     */
    public function index()
    {
        $users = $this->model->all();

        return UserResource::collection($users);
    }

    /**
     * Store a newly created user in storage.
     *
     * @param UserRequest $request
     * @return UserResource
     *
     * @apiResource App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User
     */
    public function store(UserRequest $request)
    {
        $data = $request->validated();
        $user = $this->model->create($data);

        return new UserResource($user);
    }

    /**
     * Display the specified user.
     *
     * @param  \App\Models\User  $user
     * @return UserResource
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified user in storage.
     *
     * @param UserRequest $request
     * @param \App\Models\User $user
     * @return UserResource
     */
    public function update(UserRequest $request, User $user)
    {
        $user->update($request->validated());
        return new UserResource($user);
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $user)
    {
        $user->delete();
        return $this->no_content();
    }
}
