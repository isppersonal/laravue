<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewOpportunityRequest;
use App\Http\Requests\SearchRequest;
use App\Http\Resources\OpportunityResource;
use App\Models\Opportunity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * @group Opportunities
 *
 * API endpoints for managing Opportunities
 */
class OpportunityController extends Controller
{
    protected $model;

    protected $allowedSort = ['view_count', 'id'];

    public function __construct(Opportunity $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $data = $this->model->where('user_id', $request->user()->id)
            ->paginate($request->get('page', 15));

        return OpportunityResource::collection($data);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function search(SearchRequest $request)
    {
        $search = $request->validated();

        $data = $this->model
        ->when($request->title, function ($query) use ($request) {
            $query->where('title', 'like', '%' . $request->title . '%');
        })
        ->when($request->origin_id, function ($query) use ($request) {
            $query->where('origin_id', $request->origin_id);
        })
        ->when($request->destination_id, function($query) use ($request) {
            $query->with('destinations', function($query) use ($request) {
                $query->where('city_id', $request->destination_id);
            })
            ->whereHas('destinations', function($query) use ($request) {
                $query->where('city_id', $request->destination_id);
            });
        })
        ->when($request->category_id, function($query) use ($request) {
            $query->with('categories', function($query) use ($request) {
                $query->where('category_id', $request->category_id);
            })
            ->whereHas('categories', function($query) use ($request) {
                $query->where('category_id', $request->category_id);
            });
        })
        ->when($request->order_by && in_array($request->order_by, $this->allowedSort), function($query) use ($request) {
            $ascending = intval($request->get('ascending', 0)) === 1 ? 'asc' : 'desc';
            $query->orderBy($request->order_by, $ascending);
        })
        ->paginate($request->get('limit', 15));

        return OpportunityResource::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return OpportunityResource
     */
    public function store(NewOpportunityRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = $request->user()->id;

        $opportunity = $this->model->create($data);

            foreach ($data['categories'] as $id) {
                $opportunity->categories()->create(['category_id' => $id]);
            }
            foreach ($data['destinations'] as $id) {
                $opportunity->destinations()->create(['city_id' => $id]);
            }

        return new OpportunityResource($opportunity);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Opportunity  $opportunity
     * @return OpportunityResource
     */
    public function show(Opportunity $opportunity)
    {
        return new OpportunityResource($opportunity);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Opportunity  $opportunity
     * @return OpportunityResource
     */
    public function update(Request $request, Opportunity $opportunity)
    {
        $data = $request->all();
        // TODO: check has offer then change destinations and categories
        if (isset($data['categories'])) {
            $opportunity->categories()->delete();
            foreach ($data['categories'] as $id) {
                $opportunity->categories()->create(['category_id' => $id]);
            }
        }
        if (isset($data['destinations'])) {
            $opportunity->destinations()->delete();
            foreach ($data['destinations'] as $id) {
                $opportunity->destinations()->create(['city_id' => $id]);
            }
        }

        $opportunity->update($data);

        return new OpportunityResource($opportunity);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Opportunity  $opportunity
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Opportunity $opportunity)
    {
        $opportunity->categories()->delete();
        $opportunity->destinations()->delete();
        $opportunity->offers()->delete();

        $opportunity->delete();

        return $this->no_content();
    }
}
