<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Http\Resources\OpportunityResource;
use App\Models\Opportunity;

class DebugController extends Controller
{
    public function __invoke(SearchRequest $request)
    {
        $search = $request->validated();

        $data = Opportunity::query();
//        filter by title
        if (isset($search['title'])) {
            $data->where('title', 'like', '%' . $search['title'] . '%');
        }
//        filter by origin
        if (isset($search['origin_id'])) {
            $data->where('origin_id', $search['origin_id']);
        }
//        filter by destination
        if (isset($search['destination_id'])) {
            $data->whereHas('destinations', function ($query) use ($search) {
                $query->where('city_id', $search['destination_id']);
            });
        }
//        filter by category
        if (isset($search['category_id'])) {
            $data->whereHas('categories', function ($query) use ($search) {
                $query->where('category_id', $search['category_id']);
            });
        }

        $data = $data->simplePaginate($request->get('page', 15));

        dump($data->toArray());
    }
}
