<?php

namespace App\Http\Controllers;

use App\Models\Opportunity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Orion\Http\Requests\Request;
use Orion\Concerns\DisableAuthorization;
use Orion\Http\Controllers\Controller as Controller;

class OpportunityController extends Controller
{
    use DisableAuthorization;

    protected $model = Opportunity::class;

    public function alwaysIncludes(): array
    {
        return ['user', 'destinations', 'origin', 'categories'];
    }

    public function filterableBy(): array
    {
        return ['title', 'origin_id', 'destinations.city_id', 'categories.category_id'];
    }

    /**
     * @param Request $request
     * @param Opportunity $entity
     * @return void
     */
    protected function beforeStore(Request $request, $entity)
    {
        $entity->user()->associate($request->user());
    }

    /**
     * @param Request $request
     * @param Opportunity $entity
     * @return void
     */
    protected function beforeStoreFresh(Request $request, $entity)
    {
        foreach ($request->categories as $id) {
            $entity->categories()->create(['category_id' => $id]);
        }
        foreach ($request->destinations as $id) {
            $entity->destinations()->create(['city_id' => $id]);
        }
    }
}
