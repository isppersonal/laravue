<?php

namespace App\Http\Controllers;

use App\Models\Opportunity;
use Illuminate\Http\Request;
use Orion\Concerns\DisableAuthorization;

class OpportunityDestinationsController extends \Orion\Http\Controllers\Controller
{
    use DisableAuthorization;

    protected $model = Opportunity::class;

    protected $relation = 'destinations';
}
