<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Orion\Facades\Orion;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('assets/{asset}/render', [\App\Http\Controllers\Api\Assets\RenderController::class, 'show']);
Route::delete('assets/{asset}/destroy', [\App\Http\Controllers\Api\Assets\UploadController::class, 'destroy']);

//Orion::morphToManyResource('opportunities', 'destinations', \App\Http\Controllers\OpportunityDestinationsController::class);

Route::group(['middleware' => 'guest'], function () {
    // Auth
    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', [\App\Http\Controllers\Api\AuthController::class, 'login']);
        Route::post('register', [\App\Http\Controllers\Api\AuthController::class, 'register']);
        Route::post('recover', [\App\Http\Controllers\Api\AuthController::class, 'recover']);
        Route::post('verify', [\App\Http\Controllers\Api\AuthController::class, 'verify']);
    });
    // Blog
    Route::group(['prefix' => 'blog'], function() {
        Route::apiResource('article_categories', \App\Http\Controllers\Api\Blog\ArticleCategoryController::class);
        Route::apiResource('articles', \App\Http\Controllers\Api\Blog\ArticleController::class);
        Route::apiResource('comments', \App\Http\Controllers\Api\Blog\CommentController::class);
    });
    // Contact us
    Route::apiResource('messages', \App\Http\Controllers\Api\MessageController::class);
    //Opportunities
    Route::group(['prefix' => 'opportunities'], function () {
        Route::get('search', [\App\Http\Controllers\Api\OpportunityController::class, 'search']);
        // Opportunity categories
        Route::apiResource('categories', \App\Http\Controllers\Api\CategoryController::class);
    });
    // Add offer for opportunity
//    Route::post('opportunities/{opportunity}/offer', [\App\Http\Controllers\Api\OfferController::class, 'store']);
    Route::apiResource('cities', \App\Http\Controllers\Api\CityController::class);
});

Route::group(['middleware' => ['auth:sanctum']], function () {
    // Auth
    Route::delete('/auth/logout', [\App\Http\Controllers\Api\AuthController::class, 'logout']);
    Route::get('/auth/profile', [\App\Http\Controllers\Api\AuthController::class, 'profile']);
    // User Opportunities
    Route::apiResource('opportunities', \App\Http\Controllers\Api\OpportunityController::class);
    Route::apiResource('offers', \App\Http\Controllers\Api\OfferController::class);
    Route::group(['prefix' => 'assets'], function () {
        Route::post('/', [\App\Http\Controllers\Api\Assets\UploadController::class, 'store']);
    });
    Route::apiResource('users', \App\Http\Controllers\Api\UserController::class);
});
