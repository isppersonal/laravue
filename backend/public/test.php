<?php
class User {
    public static $d = [];

    public static function phone($p)
    {
        static::$d['phone'] = $p;
        return new self();
    }
    public static function email($p)
    {
        static::$d['email'] = $p;
        return new static();
    }
    public static function company($p)
    {
        static::$d['company'] = $p;
        echo implode(' ', array_values(self::$d));
    }
}

//User::phone('09103571930')->email('email@gmail.com')->company('company');
$arr1 = [1, 2, 3];
$arr2 = ['a', 'b', 'c'];

print_r(array_merge($arr1, $arr2));
echo '<hr/>';
print_r([...$arr1, ...$arr2]);

