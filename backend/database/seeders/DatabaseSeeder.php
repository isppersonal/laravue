<?php

namespace Database\Seeders;

use App\Models\ArticleCategory;
use App\Models\Message;
use App\Models\OpportunityOffer;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(10)->create();
        Message::factory(100)->create();
        Tag::factory(100)->create();
        ArticleCategory::factory(100)->create();

        $this->call([
            ProvinceSeeder::class,
            CategorySeeder::class,
            OpportunitySeeder::class,
            ArticleSeeder::class
        ]);

//        OpportunityOffer::factory(20)->create();
    }
}
