<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Comment;
use App\Models\Tag;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = Tag::all();
        $cats = ArticleCategory::all();

        Article::factory(20)
            ->has(Comment::factory()->count(10), 'comments')
            ->create()
            ->each(function($article) use ($tags, $cats) {
                $article->tags()->sync($tags->random(rand(1,3))->pluck('id')->toArray());
                $article->categories()->sync($cats->random(rand(1, 3))->pluck('id')->toArray());
            });
    }
}
