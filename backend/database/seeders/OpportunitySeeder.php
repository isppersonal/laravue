<?php

namespace Database\Seeders;

use App\Models\Opportunity;
use App\Models\OpportunityCategory;
use App\Models\OpportunityDestination;
use App\Models\OpportunityOffer;
use Illuminate\Database\Seeder;

class OpportunitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Opportunity::factory(2)
            ->has(OpportunityCategory::factory()->count(2), 'categories')
            ->has(OpportunityDestination::factory()->count(2), 'destinations')
            ->has(OpportunityOffer::factory()->count(2), 'offers')
            ->create();
    }
}
