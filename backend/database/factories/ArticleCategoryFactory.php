<?php

namespace Database\Factories;

use App\Models\ArticleCategory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ArticleCategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->unique()->words(2, true);
        $slug = Str::slug($title, '-');
        $randomCategory = ArticleCategory::all();

        return [
            'title' => $title,
            'slug' => $slug,
            'status' => $this->faker->randomElement([0, 1]),
            'parent_id' => $randomCategory->count() > 0 ? $this->faker->randomElement([null, $randomCategory->random()->id]) : null,
            'description' => $this->faker->text(150)
        ];
    }
}
