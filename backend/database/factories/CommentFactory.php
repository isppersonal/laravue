<?php

namespace Database\Factories;

use App\Models\Comment;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $randoms = Comment::all();
        return [
            'author_name' => $this->faker->name(),
            'author_email' => $this->faker->email(),
            'subject' => $this->faker->words(mt_rand(2, 5), true),
            'content' => $this->faker->text(mt_rand(20, 150)),
            'status' => $this->faker->randomElement([0, 1]),
            'parent_id' => $randoms->count() > 0 ? $this->faker->randomElement([null, $randoms->random()->id]) : null,
            'user_id' => User::all()->random()->id,
        ];
    }
}
