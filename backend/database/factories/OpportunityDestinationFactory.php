<?php

namespace Database\Factories;

use App\Models\City;
use App\Models\Opportunity;
use Illuminate\Database\Eloquent\Factories\Factory;

class OpportunityDestinationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'opportunity_id' => Opportunity::all()->random()->id,
            'city_id' => City::all()->random()->id
        ];
    }
}
