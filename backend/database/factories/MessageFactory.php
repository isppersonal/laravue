<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MessageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            'author_name' => $this->faker->name(),
            'author_email' => $this->faker->email(),
            'subject' => $this->faker->words(mt_rand(2, 5), true),
            'content' => $this->faker->text(mt_rand(20, 150)),
            'status' => $this->faker->randomElement([0, 1]),
        ];
    }
}
