<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Opportunity;
use Illuminate\Database\Eloquent\Factories\Factory;

class OpportunityCategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'opportunity_id' => Opportunity::all()->random()->id,
            'category_id' => Category::all()->random()->id
        ];
    }
}
