<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->words(2, true);
        $slug = Str::slug($title, '-');

        return [
            'title' => $title,
            'slug' => $slug,
            'user_id' => User::all()->random()->id,
            'status' => $this->faker->randomElement([Article::STATUS_DRAFT, Article::STATUS_PUBLISHED]),
            'view_count' => $this->faker->randomDigit(),
            'like_count' => $this->faker->randomDigit(),
            'content' => $this->faker->sentence,
            'published_at' => $this->faker->dateTime('now')
        ];
    }
}
