<?php

namespace Database\Factories;

use App\Models\Opportunity;
use Illuminate\Database\Eloquent\Factories\Factory;

class OpportunityOfferFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $id = Opportunity::all()->random()->id;
        return [
            'opportunity_id' => $id,
            'offer_id' => Opportunity::all()->random()->id,
            'status' => $this->faker->randomElement([0, 1, 2, 3]),
            'description' => $this->faker->words(8, true)
        ];
    }
}
