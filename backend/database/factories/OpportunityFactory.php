<?php

namespace Database\Factories;

use App\Models\City;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class OpportunityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::all()->random()->id,
            'title' => $this->faker->company(),
            'position' => $this->faker->company(),
            'origin_id' => City::all()->random()->id,
            'description' => $this->faker->sentence()
        ];
    }
}
