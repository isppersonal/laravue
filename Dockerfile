FROM webdevops/php-nginx:8.0-alpine

ENV WEB_DOCUMENT_ROOT /app/public
ENV WEB_PHP_TIMEOUT 600
ENV SERVICE_NGINX_CLIENT_MAX_BODY_SIZE "128m"

WORKDIR /app
COPY ./backend .

RUN composer install

RUN chown -R application:application .