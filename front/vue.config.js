const path = require('path');

module.exports = {
    devServer: {
        disableHostCheck: true
    },
    configureWebpack: {
        devtool: 'source-map',
        resolve: {
            alias: {
                '@': path.resolve('src')
            },
        },
    }
}