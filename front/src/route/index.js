import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '',
        component: () => import('@/layout/Empty'),
        children: [
            {
                path: '/',
                name: 'index',
                component: () => import('@/pages/Index'),
                meta: {
                    title: 'index'
                }
            }
        ]
    },
    {
        path: '',
        component: () => import('@/layout/PagesIndex'),
        children: [
            {
                path: '/about',
                name: 'about',
                component: () => import('@/pages/About'),
                meta: {
                    title: 'about'
                }
            }
        ]
    },
    {
        path: '',
        component: () => import('@/layout/Empty'),
        children: [
            {
                path: '/opportunities',
                name: 'opportunities',
                component: () => import('@/pages/Opportunities'),
                meta: {
                    title: 'opportunities'
                }
            }
        ]
    },
]

const router = new VueRouter({
    base: '/',
    mode: 'history',
    routes: routes,
    linkActiveClass: "active",
    scrollBehavior () {
        return { x: 0, y: 0 }
    }
})

export default router