import Vue from 'vue'

Vue.filter('_ago', function (ptime) {
    if (!ptime) return ''
    let ntime = Date.parse(ptime)
    let ctime = Date.now()
    let seconds = Math.floor((ctime - ntime) / 1000),
        intervals = [Math.floor(seconds / 31536000), Math.floor(seconds / 2592000), Math.floor(seconds / 604800),
            Math.floor(seconds / 86400), Math.floor(seconds / 3600),Math.floor(seconds / 60)
        ],
        times = ['سال', 'ماه', 'هفته', 'روز', 'ساعت', 'دقیقه'],
        key, res;

    for (key in intervals) {
        if (intervals[key] >= 1){
            res  = intervals[key] + ' ' + times[key] + ' قبل';
            return res;
        }
    }
    return 'لحظاتی پیش';
})