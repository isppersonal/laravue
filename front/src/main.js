import Vue from 'vue'
import 'bootstrap'
import 'mixitup'
//jalali moment
import jMoment from 'moment-jalaali'
import moment from 'vue-moment-jalaali'
jMoment.loadPersian({ usePersianDigits: true })
Vue.use(moment)
//loading overlay
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
Vue.use(Loading, {
  color: '#5ff4f4'
})
Vue.component('loading', Loading)

import router from '@/route'

import App from './App.vue'

import '@/helpers/filters'
import  helpers from './helpers'

// StyleSheet
import 'bootstrap/dist/css/bootstrap.min.css'
import '@/assets/css/style.css'
import '@/assets/css/responsive.css'

Vue.config.productionTip = false

Vue.mixin({methods: { ... helpers }})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')