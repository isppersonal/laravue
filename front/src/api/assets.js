import apiClient from './client'

const path = '/assets'

const upload = (data) => {
    return apiClient.post(path, data);
}

export default {
    upload
}