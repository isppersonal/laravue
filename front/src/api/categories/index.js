import apiClient from '@/api/client'

const path = '/opportunities/categories'

const list = (data = {}) => {
    return apiClient.get(path, data)
}

export default{
    list
}