const key = 'user'

const setAuth = (token, info, ttl = 1440) => {
    let now = new Date()
    let auth = {
        token,
        info,
        expiry: now.getTime() + (ttl * 1000 * 60)
    }
    localStorage.setItem(key, JSON.stringify(auth))
    return getAuth()
}

const getAuth = () => {
    let auth = localStorage.getItem(key)

    if(!auth) {
        return null
    }

    let now = new Date()
    let data = JSON.parse(auth)

    if (data.expiry < now.getTime()) {
        removeAuth()
        return null
    }

    return data
}

const removeAuth = () => {
    localStorage.removeItem(key)
}

export default{
    setAuth,
    getAuth,
    removeAuth
}