import {create } from 'apisauce'
import { authStorage } from '../api'

const authKey = 'Authorization'

const apiClient = create({
    baseURL:'http://api.lara.com:8000/api',
    withCredentials: true,
})

apiClient.addAsyncRequestTransform(async (request) => {
    let auth = await authStorage.getAuth()
    if (!auth || !auth.token) return
    
    request.headers[authKey] = `Bearer ${auth.token}`
})

apiClient.addAsyncResponseTransform( async response => {
    if (response.data && response.data.data) {
        response.data = await response.data.data
        delete response.data.data
    }
})

export default apiClient