import apiClient from '@/api/client'

const path = '/cities'

const list = (data = {}) => {
    return apiClient.get(path, data)
}

export default{
    list
}