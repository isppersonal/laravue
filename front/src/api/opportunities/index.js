import apiClient from '@/api/client'

const path = '/opportunities'

const search = (data = {}) => {
    Object.keys(data).forEach(k => {
        if (data[k] === '' || parseInt(data[k]) === null) {
            delete data[k]
        }
    })
    return apiClient.get(path + '/search', data)
}

const store = (data) => {
    return apiClient.post(path, data)
}

const update = (id, data) => {
    return apiClient.put(path + '/' + id, data)
}

const show = (id) => {
    return apiClient.get(path + '/' +  + id)
}

const list = () => {
    return apiClient.get(path)
}

const destroy = (id) => {
    return apiClient.delete(path + '/' +  + id)
}

export default{
    search,
    store,
    show,
    destroy,
    update,
    list
}