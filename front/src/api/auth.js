import apiClient from "./client";

const path = '/auth/login'

const login = (data) => {
    return apiClient.get('/csrf-cookie').then(() => {
        return apiClient.post(path, data)
    })
}

const profile = () => {
    return apiClient.get('/auth/profile')
}

const logout = () => {
    return apiClient.delete('/auth/logout')
}

export default{
    login,
    profile,
    logout
}